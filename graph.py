import time
import os.path
from random import sample

import numpy as np
from scipy.sparse.csgraph import shortest_path
import networkx as nx

import torch

import torch_geometric
from torch_geometric.data import Data, Batch

def from_networkx(G):
    r"""Converts a :obj:`networkx.Graph` or :obj:`networkx.DiGraph` to a
    :class:`torch_geometric.data.Data` instance.

    Args:
        G (networkx.Graph or networkx.DiGraph): A networkx graph.
    """

    G = nx.convert_node_labels_to_integers(G);

    edge_index = torch.LongTensor(list(G.edges))
    if len(edge_index.shape) == 2:
        edge_index = torch.cat([edge_index, edge_index.flip(1)]).t().contiguous()

    data = {str(key): [] for key in G.graph['nodeattrs'] + G.graph['edgeattrs']}

    for i, (_, feat_dict) in enumerate(G.nodes(data=True)):
        for key, value in feat_dict.items():
            data[str(key)].append(value);

    for i, (_, _, feat_dict) in enumerate(G.edges(data=True)):
        for key, value in feat_dict.items():
            data[str(key)].append(value);

    for key, item in data.items():
        data[key] = torch.tensor(item)

    data['edge_index'] = edge_index.view(2, -1)
    data = Data.from_dict(data)
    data.num_nodes = G.number_of_nodes()
    
    node_attrs = [data[key] for key in G.graph['nodeattrs']]
    edge_attrs = [data[key] for key in G.graph['edgeattrs']]

    data.x = torch.stack(node_attrs).t()
    data.edge_attr = torch.stack(edge_attrs).t()

    return data

def to_nx(path, big = False):
    G = nx.Graph()

    with open(path, 'r') as f:
        content = f.readlines()

    #line1 = content[1].split(" ")
    #people = int(line1[5].replace("\n", ""))

    edge_attr = content[2].split(" ")
    edge_attr.remove('Kantenattribute:')
    num_ea = len(edge_attr)-1
    edge_attr[num_ea] = edge_attr[num_ea].replace('\n', '')

    node_attr = content[3].split(" ")
    node_attr.remove('Knotenattribute:')
    num_na = len(node_attr)-1
    node_attr[num_na] = node_attr[num_na].replace('\n', '')
    
    G.graph['nodeattrs'] = node_attr
    G.graph['edgeattrs'] = edge_attr

    content = content[5:]
    i = 0
    for line in content:
        i = i+1
        if line == "\n":
            break
        line = line.replace('\n', '')
        line = line.split(' ')
        node_id = int(line[0])
        G.add_node(node_id)

        for j in range(num_na + 1):
            G.nodes[node_id][node_attr[j]] = float(line[j+1])

    if i > 3000 and big:
        G, H = None, None
        return G, H

    content = content[i:]
    i = 0
    for line in content:
        i = i+1
        if line == "\n":
            break
        line = line.replace('\n', '')
        line = line.split(' ')
        edge_id1, edge_id2 = int(line[0]), int(line[1])
        G.add_edge(edge_id1, edge_id2)
        
        for j in range(num_ea + 1):
            G.edges[edge_id1, edge_id2][edge_attr[j]] = float(line[j+2])
    
    content = content[i:]
    
    #if not is_connected(G):
    #    largest_cc = max(nx.connected_components(G), key=len)
    #    G = G.subgraph(largest_cc).copy()

    skel = G.copy()
    
    path = []
    for line in content:
        line = line.split(' ')
        line[1] = float(line[1])
        path.append(line)
        
    path.sort(key = lambda path: path[1])
    a = int(len(path)*0.5)
    path = path[a:]

    i = 0
    for line in path:
        for j in range(len(line)-3):
            if skel.has_edge(int(line[j+2]), int(line[j+3])):
                skel.remove_edge(int(line[j+2]), int(line[j+3]))

        else:
            i = i+1

    f.close()

    return from_networkx(G), skel

# von https://github.com/rusty1s/pytorch_geometric/blob/master/examples/seal_link_pred.py
def drnl_node_labeling(edge_index, src, dst, num_nodes=None):
        # Double-radius node labeling (DRNL).
        src, dst = (dst, src) if src > dst else (src, dst)
        adj = to_scipy_sparse_matrix(edge_index, num_nodes=num_nodes).tocsr()

        idx = list(range(src)) + list(range(src + 1, adj.shape[0]))
        adj_wo_src = adj[idx, :][:, idx]

        idx = list(range(dst)) + list(range(dst + 1, adj.shape[0]))
        adj_wo_dst = adj[idx, :][:, idx]

        dist2src = shortest_path(adj_wo_dst, directed=False, unweighted=True,
                                 indices=src)
        dist2src = np.insert(dist2src, dst, 0, axis=0)
        dist2src = torch.from_numpy(dist2src)

        dist2dst = shortest_path(adj_wo_src, directed=False, unweighted=True,
                                 indices=dst - 1)
        dist2dst = np.insert(dist2dst, src, 0, axis=0)
        dist2dst = torch.from_numpy(dist2dst)

        dist = dist2src + dist2dst
        dist_over_2, dist_mod_2 = dist // 2, dist % 2

        z = 1 + torch.min(dist2src, dist2dst)
        z += dist_over_2 * (dist_over_2 + dist_mod_2 - 1)
        z[src] = 1.
        z[dst] = 1.
        z[torch.isnan(z)] = 0.

        return z.to(torch.long)

def neighborhood_split(skel, k = 4):

    node_env = []
    num_nodes = skel.number_of_nodes()
    for node in range(num_nodes):
        node_env.append(k_env(node, skel, k))

    data_list = []

    for i, (node1, node2) in enumerate(torch.combinations(torch.arange(0, num_nodes))):
        graph = skel.subgraph(node_env[node1].union(node_env[node2]))
        graph.graph['nodeattrs'] = skel.graph['nodeattrs']
        graph.graph['edgeattrs'] = skel.graph['edgeattrs']
        dat = from_networkx(graph);        
        dat.nested_batch = torch.full([dat.x.shape[0]], i);
        data_list.append(dat);

    return Batch.from_data_list(data_list), from_networkx(skel);

def k_env(node, G, k):

    nodes = {node}
    env = {node}
    new_env = {node}
    for i in range(k):
        new_env = set()
        for node1 in env:
            for neighbor in G.neighbors(node1):
                if neighbor not in nodes:
                    nodes.add(neighbor)
                    new_env.add(neighbor)
        env = new_env.copy()
        
    return nodes

def neighborhood_stitch(fake_links, stitch_info):
    new = stitch_info.clone()
    mask = fake_links[:, 0] >= 0
    new.edge_index = torch.combinations(torch.arange(0, new.num_nodes))[mask].t()
    new.edge_attr = fake_links[:, 1:][mask].clone()
    return new

def main():
    directory = "/glusterfs/dfs-gfs-dist/busdanie-pub/semesterprojekt/data/txt_files/Germany"
    nxGraphs = []
    
    start = time.time()
    
    for filename in os.listdir(directory):
        nxGraphs.append(to_nx(os.path.join(directory, filename)))
    
    env_list = []
    node_env = set()

    i = 0
    
    for G in nxGraphs:
	
        starttime = time.time()
        env_list.append([])
        G_envs = []
        
        for node in G.nodes():
            node_env = k_env(node, G, 16)
            G_envs.append([node, node_env])

        for env1 in G_envs:
            
            for env2 in G_envs:

                if env1[0] > env2[0] and not G.has_edge(env1[0], env2[0]):
                    env_list[i].append(G.subgraph(env2[1].union(env1[1])))

        print(len(env_list[i]))
        print(i+1)
        i = i+1
        endtime = time.time()
        print( '{:5.3f}s'.format(endtime-starttime))

    ende = time.time()
    print('{:5.3f}s'.format(ende-start))

    #print(env_list[0])    
    #dataGraphs = [from_networkx(G) for G in env_list[0]]
    #Daniels_Batch = from_data_list(dataGraphs)
    #print(Daniels_Batch)
    return

if __name__ == "__main__":
    main()
