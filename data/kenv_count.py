import os, sys;
sys.path.append("..");
from graph import to_nx;

total = 0;

for f in os.listdir(sys.argv[1]):
    g, _ = to_nx(os.path.join(sys.argv[1], f));
    envs = g.x.shape[0] * (g.x.shape[0] - 1);
    print(f"File {f}: {envs} k envs");
    total += envs;

print(f"Total: {total} k envs");
