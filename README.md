## Extraktion der Daten:

Auf dem Geofabrik Download Server (http://download.geofabrik.de/europe.html) finden sich OpenStreetMap-Daten zu verschiedenen Regionen. Es können die Länderdaten zu Deutschland, Frankreich und Italien verarbeitet werden. Dafür muss das vorliegende .osm.pbf-Format in ein .osm-Format
konvertiert werden. Möglichkeiten dies zu tun, sind unter https://wiki.openstreetmap.org/wiki/Osmconvert angegeben.

Das Programm ExtractData.java wandelt nun mit dem Aufruf
```
ExtractData(input_path, output_directory, country)
```
eine Länderdatei die unter input_path abgelegt ist, in mehrere .txt-Dateien um - eine je Stadt im entsprechenden Land-, die am in output_directory
angegebenen Ort gespeichert werden. Der Parameter country muss einen der Werte _germany, france_ oder _italy_ haben und gibt das Land an.
Innerhalb des Programms kann in Zeile 112 die gewünschte Mindesteinwohnerzahl der zu betrachtenden Städte verändert werden.

Im Anschluss muss das Programm UpdateFiles.py mit dem Aufruf
```
UpdateFiles.py(input_directory, output_directory_files, output_directory_metis)
```
ausgeführt werden wobei das input_directory dem output_directory des obigen Java-Programms entsprechen sollte.
Zu jeder urpsrünglichen .txt-Datei werden nun zwei neue erzeugt. Die erste beschreibt alle Kanten und Knoten des Stadtgraphen sowie deren Attribute. Alle solchen Dateien werden an jenem Ort gespeichert, der im zweiten Parameter angegeben ist.
Die zweite Textdatei enthält den Stadtgraphen im METIS-Format.
Alle solchen Dateien werden am im dritten Parameter angegeben Ort gespeichert.




## Standard Algorithmus Ausführen:
Die Graphen auf welchen der Algorithmus arbeiten soll müssen einmal im Metis Format in einem Odner vorliegen und einmal müssen die Kanten und Knoteninformationen im Format welches aus unserer Datenverarbeitungs pipeline entsteht vorliegen in einem anderen Odner.

nun muss mann in zeile 418,432 und 433 jeweils den Pfad zu dem Odner angeben in welchem die GRaphen im Metis Format vorliegen.

in Zeile 434 den Pfad zum Ordner mit den Kanten und Knoten informationen
Wichtig die datei des Metis Formats und die Datei mit den Knoteninformationen muss den selben namen haben!

in Zeile 422 gibt man den Pfad zum ordner an, in welchem man die Ausgabe von precision und recall haben möchte.

in Zeile 430 angeben, wie oft der Algorithmus durchlaufen werden soll für einen Graphen. Dieselbe anzahl muss auch in Zeile 456 jeweils im Nenner stehen.

Beim Algorithmus gibt es einige Stellschrauben die die Performance beeinflussen:

Treshhold zum Annehmen einer Kante -> änderbar in Zeile 373
Anzahl der Schritte die beim Random Walk gelaufen werden -> änderbar in Zeile 130
Wie oft der Random Walk von einem Knoten jeweils ausgeführt werden soll ->änderbar in Zeile 366
Und wie viel % der Knoten beibehalten werden sollen -> änderbar in zeile 339.

Wenn man den Algorithmus für einen einzigen GRaphen nur einmal laufen lässt kann man in Zeile 404 sich immer ausgeben lassen, welche Kanten hinzugefügt worden sind.

Hat man alle Stellschrauben und Pfade eingegeben, kann man das den Algorithmus mit python3 von der Konsole aus aufrufen.
