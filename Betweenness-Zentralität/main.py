from gnn import gnn
from mlp import mlp
import networkit as nk
import torch
from scipy.stats import kendalltau
import os

def train(layers,epoch,unit_layers,G_list,G_list2):
    misc=[]
    for j in range(len(G_list)):
        misc.append(gnn(G_list[j]))
        for i in range(layers):
            misc[j].newLayer()
    unit = mlp(unit_layers,layers)
    nodecount=0
    for i in range(len(misc)):
            nodecount+=misc[i].graph.numberOfNodes()
    x_train=torch.FloatTensor(nodecount,layers+1)
    y_train=torch.FloatTensor(nodecount)
    nodenr=0
    for i in range(len(misc)): 
        x_temp=misc[i].layer_out()
        y_temp=misc[i].score_out()
        for j in range(misc[i].graph.numberOfNodes()):
            x_train[j+nodenr]=x_temp[j]
            y_train[j+nodenr]=y_temp[j]
        nodenr+= misc[i].graph.numberOfNodes()
    criterion = torch.nn.MSELoss()
    optimizer = torch.optim.Rprop(unit.parameters(),0.01,(0.5, 1.2),(1e-06, 500))
    unit.train()
    for epoch in range(epoch):
        optimizer.zero_grad()
        y_pred=torch.FloatTensor(nodecount)
        for i in range(nodecount):
                y_pred[i]= unit.forward(x_train[i])    
        loss = criterion(y_pred.squeeze(), y_train)  # Backward pass
        loss.backward()
        optimizer.step()
    misc2=[]
    nodecount=0
    for j in range(len(G_list2)):
        misc2.append(gnn(G_list2[j]))
        nodecount+=G_list2[j].numberOfNodes()
        for i in range(layers):
            misc2[j].newLayer()
    
    for i in range(len(misc2)):
            nodecount+=misc2[i].graph.numberOfNodes()
    x_train=torch.FloatTensor(nodecount,layers+1)
    y_train=torch.FloatTensor(nodecount)
    nodenr=0
    for i in range(len(misc2)): 
        x_temp=misc2[i].layer_out()
        y_temp=misc2[i].score_out()
        for j in range(misc2[i].graph.numberOfNodes()):
            x_train[j+nodenr]=x_temp[j]
            y_train[j+nodenr]=y_temp[j]
        nodenr+= misc2[i].graph.numberOfNodes()
    y_pred=torch.FloatTensor(nodecount)
    for i in range(nodecount):
        y_pred[i]= unit.forward(x_train[i])
    kt=kendalltau(y_pred.detach().numpy(),y_train.detach().numpy())
    print('GNN-Layer{}: MLP-Layer: {} {}'.format(layers,unit_layers,kt))

path="/glusterfs/dfs-gfs-dist/busdanie-pub/semesterprojekt/data/txt_files/Germany_02_07/Germany_02_07_adjacency_lists/"

test=[]
for root, dirs, files in os.walk(path):
    for f in files:
        test.append(nk.readGraph(os.path.join(root, f), nk.Format.METIS))

G=nk.readGraph("Aachen_0.txt", nk.Format.METIS)
training=[G]

G2=nk.readGraph("Leipzig_0.txt", nk.Format.METIS)


print("Data loaded")
for i in range(10):
    for j in range(10):
        train(1+i,20,1+j, training, test)
