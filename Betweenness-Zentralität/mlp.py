import torch
class mlp(torch.nn.Module):
        def __init__(self,hidden,layers):
                super(mlp, self).__init__()
                self.layers=layers
                self.hidden_size1 = hidden
                self.hidden_size2 = hidden
                self.map1 = torch.nn.Linear(self.layers+1, self.hidden_size1)
                self.map2 = torch.nn.Linear(self.hidden_size1, self.hidden_size2)
                self.map3 = torch.nn.Linear(self.hidden_size2, 1)
                self.relu = torch.nn.ReLU()
        def forward(self, x):
                hidden1  = self.map1(x)
                relu1    = self.relu(hidden1)
                hidden2  = self.map2(relu1)
                relu2=self.relu(hidden2)
                return self.map3(relu2)
