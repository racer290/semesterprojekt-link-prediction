import networkit as nk
import torch
class gnn:
    def __init__(self,graph):
        self.layers=[]
        self.graph=graph
        self.layers.append([])
        self.layercount=0
        for u in self.graph.iterNodes():            
            if self.graph.degree(u)== 1:
               self.layers[0].append(0) 
            else:
                self.layers[0].append(self.graph.degree(u))
    def newLayer(self):
        self.layercount += 1
        self.layers.append([])
        for u in self.graph.iterNodes():            
            self.layers[self.layercount].append(0)
        for u, w in self.graph.iterEdges():
            self.layers[self.layercount][u]+=self.layers[self.layercount-1][w]
            self.layers[self.layercount][w]+=self.layers[self.layercount-1][u]
        for u in self.graph.iterNodes():            
            if self.layers[self.layercount-1][u]==0:
                self.layers[self.layercount][u]=0
    def printlayer(self,i):
        for u in self.layers[i]:
            print (u)
    def layer_out(self):
        x=torch.FloatTensor(self.graph.numberOfNodes(),self.layercount+1)
        for i in range(self.graph.numberOfNodes()):
            for j in range(self.layercount+1):
                x[i][j]=self.layers[j][i]
        return x
    def score_out(self):
        btwn = nk.centrality.Betweenness(self.graph)
        btwn.run()
        y=torch.FloatTensor(self.graph.numberOfNodes()) #todo make this work for bad build graphs
        for i in range(self.graph.numberOfNodes()):
            y[i]=btwn.score(i)
        return y
