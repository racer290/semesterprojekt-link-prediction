from graph import neighborhood_split, neighborhood_stitch, to_nx as to_networkx, from_networkx;
from net import *;
from torch_geometric.data import Batch;

torch.manual_seed(242424242);
torch.multiprocessing.set_start_method('fork');
torch.multiprocessing.set_sharing_strategy('file_system');
device = torch.device("cpu");

dat = to_networkx("dataset/raw/Adenau_0.txt")[0];
batch = Batch.from_data_list([dat]);

linkpredictor = LinkPredictorNetwork(batch.num_node_features, [30, 20, 10], 1 + batch.num_node_features).to(device);
discriminator = DiscriminatorNetwork(batch.num_node_features, [100, 50, 20], 1).to(device);

print(linkpredictor(batch.x, batch.edge_index, batch.batch));
print(discriminator(batch.x, batch.edge_index, batch.batch));

print("simple test run done");

linkpredictor.train();
discriminator.train();
link_optimizer = torch.optim.Adam(linkpredictor.parameters(), lr=0.0005);
disc_optimizer = torch.optim.Adam(discriminator.parameters(), lr=0.0005);

skel_graphs = batch;
real_graphs = batch;

lossfn = BCELoss();

print("setup complete");
num_fakes = skel_graphs.num_graphs;
num_reals = real_graphs.num_graphs;

skel_batch, stitch_info = neighborhood_split(skel_graphs, 4);
fake_links = linkpredictor(skel_batch.x, skel_batch.edge_index, skel_batch.batch);
fake_graphs = neighborhood_stitch(fake_links, stitch_info);

disc_optimizer.zero_grad();

fake_results = discriminator(fake_graphs.x, fake_graphs.edge_index, fake_graphs.batch);
real_results = discriminator(real_graphs.x, real_graphs.edge_index, real_graphs.batch);
fake_loss = lossfn(fake_results, torch.zeros(num_fakes, 1).to(device));
real_loss = lossfn(real_results, torch.ones(num_reals, 1).to(device));

fake_loss.backward();
real_loss.backward();
disc_optimizer.step();
disc_loss = fake_loss + real_loss;

print("discriminator test run done");
# train linkpredictor
link_optimizer.zero_grad();

skel_batch, stitch_info = neighborhood_split(skel_graphs, 4);
links = linkpredictor(skel_batch.x, skel_batch.edge_index, skel_batch.batch);
output = neighborhood_stitch(links, stitch_info);

results = discriminator(output.x, output.edge_index, output.batch);
link_loss = lossfn(results, torch.zeros(num_fakes, 1).to(device));

link_loss.backward();
link_optimizer.step();
print("link predictor test run done");
print("finished");
