#!/usr/bin/env python3
#-*- coding:utf-8 -*-
from networkit import *
import numpy as np
from numpy.core.numeric import True_
from numpy.lib.function_base import append, average
from numpy.linalg import norm
import random 
from collections import Counter 
from math import radians, cos, sin, asin, sqrt
import os



class knotenattribute: #objekt welches knotenattribute beinhaltet

    def __init__ (self,knotennr, lon , lat, traffic_signals, average_lanes , average_maxspeed ):
        self.knotennr=int(knotennr)
        self.lon=float(lon)
        self.lat=float(lat)
        self.signals=int(traffic_signals)
        self.lanes=int(average_lanes)
        self.maxspeed=int(average_maxspeed)

class kantenattribute:
    def __init__(self,knotena, knotenb,length,roadtype,maxspeed, lanes, motorroad , oneway, priorityroad ,traffic_calming): 
        self.knotena=int(knotena)
        self.knotenb=int(knotenb)
        self.length=float(length)
        self.roadtype=int(roadtype)
        self.maxspeed=int(maxspeed)
        self.lanes=int(lanes)
        self.motorroad=int(motorroad)
        self.oneway=int(oneway)
        self.priority=int(priorityroad)
        self.calming=int(traffic_calming)


def knotenattributreader(atrributliste): 
    attri=int(5)
    number=int(g.numberOfNodes()+5)
    while attri < number:
        line=atrributliste[attri]
        line=line.replace("lon:","") #Zeile 30-34 loescht lon, lat etc. da nur die zahlen gebraucht werden
        line=line.replace("lat:","" )
        line=line.replace("traffic_signals:","" )
        line=line.replace("average_lanes:","" )
        line=line.replace("average_maxspeed:","" )
        line=line.replace("\n","")
        brauchbar=line.split(" ")
        attri=attri+1

        knotenmatrix.append(knotenattribute(brauchbar[0],brauchbar[1],brauchbar[2],brauchbar[3],brauchbar[4],brauchbar[5]))
       
def knoteninformationen(knotennummer):# Gibt fÃƒÂ¼r eine Knoten ID die Attribute aus
    if knotennummer< 0 or knotennummer > g.numberOfNodes():
        print("UngÃƒÂ¼ltige Knotennummer")
        return none 
        
    print("Knotennummer:",knotenmatrix[knotennummer].knotennr)
    print("LÃƒÂ¤ngengrad (lon):",knotenmatrix[knotennummer].lon)
    print("Breitengrad (lat):",knotenmatrix[knotennummer].lat)
    print("traffic Signals:",knotenmatrix[knotennummer].signals)
    print("Average Lanes:",knotenmatrix[knotennummer].lanes)
    print("Max Speed:",knotenmatrix[knotennummer].maxspeed)



def kanteninformationen(knotena,knotenb):# Gibt fÃƒÂ¼r eine Knoten ID die Attribute aus
    
        
    
    print("LÃƒÂ¤nge:",kantenattributmatrix[knotena][knotenb].length)
    print("Roadtype:",kantenattributmatrix[knotena][knotenb].roadtype) 
    print("Max Speed:",kantenattributmatrix[knotena][knotenb].maxspeed)
    print("Lanes:",kantenattributmatrix[knotena][knotenb].lanes)
    print("Motorroad:",kantenattributmatrix[knotena][knotenb].motorroad)
    print("Oneway:",kantenattributmatrix[knotena][knotenb].oneway)  
    print("Priority Road:",kantenattributmatrix[knotena][knotenb].priority)
    print("Traffic Calming:",kantenattributmatrix[knotena][knotenb].calming)
    
def attributdistanz(knotena , knotenb): #berechnet euklidische distanz von 2 Knotenvekoten und gibt diese zurÃƒÂ¼ck
    #print("a:",knotena)
    #print("b:",knotenb)
    knotenarraya=np.array([knotenmatrix[knotena].lon, knotenmatrix[knotena].lat, knotenmatrix[knotena].signals, knotenmatrix[knotena].lanes, knotenmatrix[knotena].maxspeed])
    knotenarrayb=np.array([knotenmatrix[knotenb].lon, knotenmatrix[knotenb].lat, knotenmatrix[knotenb].signals, knotenmatrix[knotenb].lanes, knotenmatrix[knotenb].maxspeed])
    
    return norm(knotenarraya-knotenarrayb)


class knotenwarscheinlichkeit:
    def __init__(self,knotennr,prob):
        self.knotennr=int(knotennr)
        self.prob=prob
        

def matrixfueller(): #berechnet ÃƒÂ¼bergangswarscheinlichkeiten
    
    for x in g.iterNodes():
        
        for y in g.iterNeighbors(x): #schreibt distanz der Knoten in die das richtige Feld der ÃƒÂ¼bergangsmatrix
                #print("Distanz von Knoten ",x ,"und knoten",y ,"entspricht", attributdistanz(y,x))
                uebergangsmatrix[x,y]=attributdistanz(x,y)
    for x in range(g.numberOfNodes()): #berechnet zeilensumme
            zeilensumme=np.sum(uebergangsmatrix,axis=1)
            
    for x in range(g.numberOfNodes()): 
        for y in range(g.numberOfNodes()): 
            if uebergangsmatrix[x,y]!=0:
               
                uebergangsmatrix[x,y]=(zeilensumme[x]-uebergangsmatrix[x,y])
                
    
    for x in range(g.numberOfNodes()): #berechnet zeilensumme
            zeilensumme2=np.sum(uebergangsmatrix,axis=1)
            
    for x in range(g.numberOfNodes()): #schreibt die endgÃƒÂ¼ltigen ÃƒÂ¼bergangswarscheinlichkeiten in die matrix
        for y in range(g.numberOfNodes()): 
            if uebergangsmatrix[x,y]!=0:
               
                uebergangsmatrix[x,y]=(uebergangsmatrix[x,y]/zeilensumme2[x])
                

                
        

def randomwalker(startknoten,reichweite): 
    wiederholung=reichweite
    startknoten=startknoten
    if wiederholung == 2: # Hier kann die reichweite gesteuert werden, wie viele schritte man maximal lauft!
        
        return startknoten
    
    knotenprob=[]
    
    
    for x in g.iterNeighbors(startknoten):
        knotenprob.append(knotenwarscheinlichkeit(knotenmatrix[x].knotennr, uebergangsmatrix[startknoten,x])) 
        
        
        
    knotenprob.sort(key=lambda x : x.prob ,reverse=True)
    randomzahl=random.random()
    warscheinlichkeit=0
    for x in knotenprob:
        #print(x.prob)
        warscheinlichkeit=warscheinlichkeit+x.prob
        if randomzahl <= warscheinlichkeit:  
            #print("Schritt Nr", reichweite)  
            return randomwalker(x.knotennr,reichweite+1)
        
        
def kantenattributreader(attributliste):
    
    for x in range(g.numberOfNodes()+6,(g.numberOfNodes()+5)+g.numberOfEdges()):
        line=attributliste[x]
        line=line.replace("length:","")
        line=line.replace("roadtype:","")
        line=line.replace("maxspeed:","")
        line=line.replace("lanes:","")
        line=line.replace("motorroad:","")
        line=line.replace("oneway:","")
        line=line.replace("priority_road:","")
        line=line.replace("traffic_calming:","")
        line=line.replace("\n","")
        brauchbar=line.split(" ")
        #print(brauchbar, " x:" , x)
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].knotena=brauchbar[0]
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].knotenb=brauchbar[1]
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].length=brauchbar[2]
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].roadtype=brauchbar[3]
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].maxspeed=brauchbar[4]
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].lanes=brauchbar[5]
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].motorroad=brauchbar[6]
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].oneway=brauchbar[7]
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].priority=brauchbar[8]
        kantenattributmatrix[int(brauchbar[0])][int(brauchbar[1])].calming=brauchbar[9]
        

        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].knotena=brauchbar[0]
        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].knotenb=brauchbar[1]
        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].length=brauchbar[2]
        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].roadtype=brauchbar[3]
        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].maxspeed=brauchbar[4]
        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].lanes=brauchbar[5]
        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].motorroad=brauchbar[6]
        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].oneway=brauchbar[7]
        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].priority=brauchbar[8]
        kantenattributmatrix[int(brauchbar[1])][int(brauchbar[0])].calming=brauchbar[9]
   
def most_frequent(Liste):
    
    counter = 0
    
    num = Liste[0]
     
    for i in Liste:
        curr_frequency = Liste.count(i)
        if(curr_frequency> counter):
            counter = curr_frequency
            num = i
    
    return int(num)

def lanes(knotena,knotenb):
    lanesliste=[]
    for x in g.iterNeighbors(knotena):
        
        lanesliste.append(int(kantenattributmatrix[knotena][x].lanes))
        
    
    for y in g.iterNeighbors(knotenb):
        
        lanesliste.append(int(kantenattributmatrix[y][knotenb].lanes))
        
    
   
    
    return most_frequent(lanesliste)

def maxspeed(knotena,knotenb):
    lanesliste=[]
    for x in g.iterNeighbors(knotena):
        
        lanesliste.append(int(kantenattributmatrix[knotena][x].maxspeed))
        
    
    for y in g.iterNeighbors(knotenb):
        
        lanesliste.append(int(kantenattributmatrix[y][knotenb].maxspeed))
        
    
    
    
    return most_frequent(lanesliste) 

def roadtype(knotena,knotenb):
    lanesliste=[]
    for x in g.iterNeighbors(knotena):
        
        lanesliste.append(int(kantenattributmatrix[knotena][x].roadtype))
        
    
    for y in g.iterNeighbors(knotenb):
        
        lanesliste.append(int(kantenattributmatrix[y][knotenb].roadtype))
        
    
    
    
    return most_frequent(lanesliste)

def priolane(knotena,knotenb):
    lanesliste=[]
    for x in g.iterNeighbors(knotena):
        
        lanesliste.append(int(kantenattributmatrix[knotena][x+1].priority))
        
    
    for y in g.iterNeighbors(knotenb):
        
        lanesliste.append(int(kantenattributmatrix[y+1][knotenb].priority))
        
    if 1 in lanesliste:
        return 1
    else:
        return 0

def motorroad(knotena,knotenb):

    lanesliste=[]
    for x in g.iterNeighbors(knotena):
        
        lanesliste.append(int(kantenattributmatrix[knotena][x].motorroad))
        
    
    for y in g.iterNeighbors(knotenb):
        
        lanesliste.append(int(kantenattributmatrix[y][knotenb].motorroad))
        
    if 1 in lanesliste:
        return 1
    else:
        return 0       

def oneway(knotena,knotenb):
    lanesliste=[]
    for x in g.iterNeighbors(knotena):
        
        lanesliste.append(int(kantenattributmatrix[knotena][x].oneway))
        
    
    for y in g.iterNeighbors(knotenb):
        
        lanesliste.append(int(kantenattributmatrix[y][knotenb].oneway))
        
    if 1 in lanesliste:
        return 1
    else:
        return 0

def calming(knotena,knotenb):
    lanesliste=[]
    for x in g.iterNeighbors(knotena):
        
        lanesliste.append(int(kantenattributmatrix[knotena][x].calming))
        
    
    for y in g.iterNeighbors(knotenb):
        
        lanesliste.append(int(kantenattributmatrix[y][knotenb].calming))
        
    if 1 in lanesliste:
        return 1
    else:
        return 0

def distanz(lon1, lat1, lon2, lat2):
   
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    r = 6371 # Radius of earth in kilometers. Use 3956 for miles
    return c * r

def wegeweg():
    pfade=[]
    for x in range((g.numberOfNodes()+7)+g.numberOfEdges(),len(lines)):
        tmp=lines[x].split(' ')
        tmp[1]=float(tmp[1])
        pfade.append(tmp)

    pfade.sort(key=lambda pfade:pfade[1])    
    a=int(len(pfade)*0.75) # einstellen wie viel % der wege beibehalten werden soll.
    pfade=pfade[a:]
    
    vorher =g.numberOfEdges()
    for tmp in pfade:
        i=2 
        j=3
        while j<len(tmp):
            if g.hasEdge(int(tmp[i]),int(tmp[j])):
                g.removeEdge(int(tmp[i]),int(tmp[j]))
                i=i+1
                j=j+1
            else:
                i=i+1
                j=j+1
    nacher=g.numberOfEdges()
    return vorher-nacher

def main():
        
        fp=0
        fn=0
        truepositive=float(0)
        kantenhinzugefuegt=0    
        for x in g.iterNodes():# segmentation fault
        
            
            ergebnis=[randomwalker(x,0)for y in range(25)]#range hier einstellen wie oft von einem knoten aus gelaufen werden soll
            
            ergebniswo = list(filter(None, ergebnis))
            
            if len(ergebniswo)>0:
                zielknoten=int(most_frequent(ergebniswo))
                
                if  attributdistanz(zielknoten,x)<1000  and (not g.hasEdge(zielknoten,x)) and not zielknoten==x: #bei attributdistanz treshhold einstellen wie aehnlich sich knoten sein mÃƒÂ¼ssen um eine kante zu erhalten
                    
                    g.addEdge(zielknoten,x)
                    fn=fn+1
                    if kontrollgraph.hasEdge(zielknoten,x):
                        truepositive=truepositive+1
                    else:
                        fp=fp+1
                    kantenhinzugefuegt=kantenhinzugefuegt+1
                    
                    kantenattributmatrix[zielknoten][x].length=distanz(knotenmatrix[x].lon,knotenmatrix[x].lat,knotenmatrix[zielknoten].lon,knotenmatrix[zielknoten].lat)
                    kantenattributmatrix[zielknoten][x].roadtype=roadtype(zielknoten,x)
                    kantenattributmatrix[zielknoten][x].priority=priolane(zielknoten,x)         
                    kantenattributmatrix[zielknoten][x].maxspeed=maxspeed(zielknoten,x)
                    kantenattributmatrix[zielknoten][x].calming=calming(zielknoten,x)
                    kantenattributmatrix[zielknoten][x].lanes=lanes(zielknoten,x)
                    kantenattributmatrix[zielknoten][x].oneway=oneway(zielknoten,x)
                    kantenattributmatrix[zielknoten][x].motorroad=motorroad(zielknoten,x)
                    kantenattributmatrix[zielknoten][x].knotena=x
                    kantenattributmatrix[zielknoten][x].knotenb=zielknoten

                    kantenattributmatrix[x][zielknoten].length=distanz(knotenmatrix[x].lon,knotenmatrix[x].lat,knotenmatrix[zielknoten].lon,knotenmatrix[zielknoten].lat)
                    kantenattributmatrix[x][zielknoten].roadtype=roadtype(zielknoten,x)
                    kantenattributmatrix[x][zielknoten].priority=priolane(zielknoten,x)         
                    kantenattributmatrix[x][zielknoten].maxspeed=maxspeed(zielknoten,x)
                    kantenattributmatrix[x][zielknoten].calming=calming(zielknoten,x)
                    kantenattributmatrix[x][zielknoten].lanes=lanes(zielknoten,x)
                    kantenattributmatrix[x][zielknoten].oneway=oneway(zielknoten,x)
                    kantenattributmatrix[x][zielknoten].motorroad=motorroad(zielknoten,x)
                    kantenattributmatrix[x][zielknoten].knotena=x
                    kantenattributmatrix[x][zielknoten].knotenb=zielknoten
                    #print("Die Kante (",x , "," ,zielknoten , " wurde hinzugefÃƒÂ¼gt")
                    #kanteninformationen(x,zielknoten)
        #print("Insgesamt Kanten hinzugefÃƒÂ¼gt",kantenhinzugefuegt)
        #print(fp)
        #print(truepositive)
        if(truepositive+fp)!=0:
            precision= float(truepositive/(truepositive+fp))
        else:
            precision=0    
        returnliste=[precision , truepositive]
        return returnliste



os.chdir("/glusterfs/dfs-gfs-dist/busdanie-pub/semesterprojekt/testsetgros/")
os.getcwd

testliste=os.listdir()
os.chdir("/glusterfs/dfs-gfs-dist/busdanie-pub/semesterprojekt/")
with open('Ausgabe.txt','w') as f:
    f.write("Datei Stadtgroesse Precision Recall" + "\n")

for y in testliste:  
    print(y)
    averagerecall=0
    averageprecision=0
    for x in range(5):
            print(x+1)
            kontrollgraph=readGraph("/glusterfs/dfs-gfs-dist/busdanie-pub/semesterprojekt/testsetgros/"+ y,Format.METIS)
            g = readGraph("/glusterfs/dfs-gfs-dist/busdanie-pub/semesterprojekt/testsetgros/"+y,Format.METIS) #liest graph ein aus Metis format
            attributdatei = open("/glusterfs/dfs-gfs-dist/busdanie-pub/semesterprojekt/data/txt_files/Germany_02_07/Germany_02_07_neue_IDs/"+y,"r")
            lines= attributdatei.readlines()#liste mit allen zeilen aus der attribute datei
            uebergangsmatrix= np.zeros((g.numberOfNodes(),g.numberOfNodes()))#ÃƒÂ¼bergangsmatrix fÃƒÂ¼r random walk
            knotenmatrix = [] # liste mit allen knoten und knoteninformationen
            global truepositive
            global fp

            kantenattributmatrix = [[kantenattribute(0,0,0,0,0,0,0,0,0,0) for j in range(g.numberOfNodes()+1)] for i in range(g.numberOfNodes()+1)]
            knotenattributreader(lines)


            matrixfueller()

            kantenattributreader(lines)
            relevantgesamt=wegeweg()
            precisionundtp=main()
            recall= float(precisionundtp[1]/relevantgesamt)

            averagerecall=averagerecall+recall
            averageprecision=precisionundtp[0]+averageprecision
    


    with open('Ausgabe.txt','a') as f:
                f.write( y + " "+ str(kontrollgraph.numberOfNodes()) +  str(averageprecision/5) +" " + str(averagerecall/5) +'\n')
   # print("Precision:",precision)

    attributdatei.close
   
